import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/11/2.
 */
public class RequestObject implements Serializable {
    public static final int REQ_REG = 1;
    public static final int REQ_LOG = 2;
    public static final int REQ_LOADFRIENDLIST = 3;
    public static final int REQ_ADDNEWFRIEND = 4;
    public static final int REQ_CONFIRMADDNEWFRIEND = 5;
    public static final int REQ_SENDMESSAGE = 6;
    public static final int REQ_LOADHISTORYCHAT = 7;
    public static final int REQ_CREATENEWGROUP = 8;
    public static final int REQ_LOADGROUPLIST = 9;
    public static final int REQ_LOADGROUPHISTORYCHAT = 10;
    public static final int REQ_SENDGROUPMESSAGE = 11;
    private int reqType;
    private Object reqBody;
    public RequestObject(int reqType,Object reqBody){
        super();
        this.reqType = reqType;
        this.reqBody = reqBody;
    }
    public int getReqType(){
        return reqType;
    }
    public Object getReqBody(){
        return reqBody;
    }
    @Override
    public String toString(){
        return "Request Type: " + reqType + " .Request Content: " + reqBody;
    }
}
