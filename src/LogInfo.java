import java.io.Serializable;

public class LogInfo implements Serializable{
    private String username;
    private String password;
    public LogInfo(String name, String pass){
        username = name;
        password = pass;
    }
    @Override
    public String toString(){
        return "username is " + username + ", password is " + password;
    }
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }
}
