import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Created by Administrator1 on 2016/11/27.
 */
public class AddNewFriendGUI{
    public AddNewFriendGUI(Stage primaryStage){
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("Add new friend!");
        primaryStage.setResizable(false);
        int disWidth = 350;
        int disHeight = 260;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(15);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Label scenetitle = new Label("Enter his/her user name: ");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,16));
        grid.add(scenetitle,0,0,2,1);
        TextField tf = new TextField();
        grid.add(tf,0,1,3,1);
        Button but1 = new Button("Cancel");
        grid.add(but1,0,2);
        Button but2 = new Button("OK");
        HBox hbbut2 = new HBox();
        hbbut2.setAlignment(Pos.BOTTOM_RIGHT);
        hbbut2.getChildren().add(but2);
        grid.add(hbbut2,2,2);
        Text errText = new Text();
        errText.setFill(Color.FIREBRICK);
        grid.add(errText,2,3);
        but1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.hide();
            }
        });
        but2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tf.getText().isEmpty()){
                    errText.setText("You must input User Name!");
                    return;
                }
                RequestObject reqObj = new RequestObject(RequestObject.REQ_ADDNEWFRIEND, new NewFriendInfo(tf.getText()));
                RequestHandler.getReqHdler().sendMessage(reqObj);
                primaryStage.hide();
            }
        });
    }
}
