import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/12/25.
 */
public class NewGroupInfo implements Serializable {
    private String groupName;
    private int[] groupmembers;

    public NewGroupInfo(String name, int[] members){
        groupName = name;
        groupmembers = members;
    }

    @Override
    public String toString(){
        return "group name is " + groupName + ", number of members is " + (groupmembers.length + 1 );
    }

    public String getGroupName(){
        return groupName;
    }

    public int[] getGroupmembers(){
        return groupmembers;
    }
}
