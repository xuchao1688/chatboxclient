/**
 * Created by Administrator1 on 2016/11/9.
 */

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.*;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.io.File;


public class ChatGUI {
    private int currChatuid = -1;
    private int currChatGroupid = -1;
    static class ContactCell extends ListCell<String>{
        GridPane grid = new GridPane();
        ImageView iv = new ImageView(new Image("UserIcon.jpg"));
        HBox hbiv = new HBox();
        Label lab1 = new Label();
        Label lab2 = new Label();
        String lastItem;
        private String[] username;
        private int[] useronline;
        public ContactCell(){
            super();
            iv.setPreserveRatio(true);
            iv.setFitHeight(40);
            hbiv.getChildren().add(iv);
            hbiv.setPadding(new Insets(0,10,0,0));
            grid.add(hbiv,0,0,1,2);
            grid.add(lab1,1,0);
            grid.add(lab2,1,1);
        }
        @Override
        protected void updateItem(String item, boolean empty){
            super.updateItem(item,empty);
            setText(null);
            if(empty){
                lastItem = null;
                setGraphic(null);
            }else{
                lastItem = item;
                if(item == null){
                    HBox hb = new HBox();
                    hb.getChildren().add(new Label("<empty>"));
                    setGraphic(hb);
                }else{
                    lab1.setText(username[Integer.parseInt(item)]);
                    lab2.setText(useronline[Integer.parseInt(item)] == 1 ? "online" : "offline");
                    setGraphic(grid);
                }
            }
        }
        public void setUsername(String[] name){
            username = name;
        }
        public void setUseronline(int[] online){
            useronline = online;
        }
    }
    static class JoinedGroupCell extends ListCell<String>{
        GridPane grid = new GridPane();
        ImageView iv = new ImageView(new Image("GroupIcon.png"));
        HBox hbiv = new HBox();
        Label lab1 = new Label();
        Label lab2 = new Label();
        String lastItem;
        private String[] groupname;
        private int[] groupmembers;
        public JoinedGroupCell(){
            super();
            iv.setPreserveRatio(true);
            iv.setFitHeight(40);
            hbiv.getChildren().add(iv);
            hbiv.setPadding(new Insets(0,10,0,0));
            grid.add(hbiv,0,0,1,2);
            grid.add(lab1,1,0);
            grid.add(lab2,1,1);
        }
        @Override
        protected void updateItem(String item, boolean empty){
            super.updateItem(item,empty);
            setText(null);
            if(empty){
                lastItem = null;
                setGraphic(null);
            }else{
                lastItem = item;
                if(item == null){
                    HBox hb = new HBox();
                    hb.getChildren().add(new Label("<empty>"));
                    setGraphic(hb);
                }else{
                    lab1.setText(groupname[Integer.parseInt(item)]);
                    lab2.setText(Integer.toString(groupmembers[Integer.parseInt(item)]) + " members");
                    setGraphic(grid);
                }
            }
        }
        public void setGroupname(String[] name){
            groupname = name;
        }
        public void setGroupmembers(int[] members){
            groupmembers = members;
        }
    }
    public ChatGUI (Stage primaryStage, int id ) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("ChatBox");
        primaryStage.setResizable(false);
        int disWidth = 800;
        int disHeight = 600;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        VBox vbleft = new VBox();
        vbleft.setAlignment(Pos.CENTER);
        HBox hbfuncButs = new HBox();
        ImageView iv = new ImageView(new Image("NewContact.png"));
        iv.setFitWidth(30);
        iv.setFitHeight(30);
        Button addNewContacterBut = new Button();
        addNewContacterBut.setGraphic(iv);
        iv = new ImageView(new Image("NewGroup.png"));
        iv.setFitWidth(30);
        iv.setFitHeight(30);
        Button createNewGroupBut = new Button();
        createNewGroupBut.setGraphic(iv);
        iv = new ImageView(new Image("Contact.png"));
        iv.setFitWidth(30);
        iv.setFitHeight(30);
        Button showContactListBut = new Button();
        showContactListBut.setGraphic(iv);
        iv = new ImageView(new Image("Group.png"));
        iv.setFitWidth(30);
        iv.setFitHeight(30);
        Button showGroupListBut = new Button();
        showGroupListBut.setGraphic(iv);
        hbfuncButs.getChildren().addAll(addNewContacterBut,createNewGroupBut,showContactListBut,showGroupListBut);
        vbleft.getChildren().add(hbfuncButs);
        VBox vbRight = new VBox();
        vbRight.setAlignment(Pos.CENTER);
        ScrollPane chatContentPane = new ScrollPane();
        chatContentPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        chatContentPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        chatContentPane.setMaxSize(5*primaryStage.getWidth()/7,5*primaryStage.getHeight()/7);
        chatContentPane.setPrefSize(5*primaryStage.getWidth()/7,5*primaryStage.getHeight()/7);
        GridPane chatContent = new GridPane();
        chatContentPane.setContent(chatContent);
        chatContentPane.setPadding(new Insets(5,10,0,10));
//        chatContent.setMaxSize(5*primaryStage.getWidth()/7,5*primaryStage.getHeight()/7);
//        chatContent.setPrefSize(5*primaryStage.getWidth()/7,5*primaryStage.getHeight()/7);
//        chatContent.setWrapText(true);
//        chatContent.setEditable(false);
        vbRight.getChildren().add(chatContentPane);
        TextArea chatMessage = new TextArea();
        chatMessage.setMaxSize(5*primaryStage.getWidth()/7,10*primaryStage.getHeight()/49);
        chatMessage.setPrefSize(5*primaryStage.getWidth()/7,10*primaryStage.getHeight()/49);
        chatMessage.setWrapText(true);
        vbRight.getChildren().add(chatMessage);
        Button sendBut = new Button("Send");
        Button sendfileBut = new Button("Send file");
        Button sendRecordBut = new Button("Send Voice");
        Button sendPicBut = new Button("Send Pic");
        FileChooser fileChooser = new FileChooser();
        HBox hbsendBut = new HBox();
        hbsendBut.setAlignment(Pos.BOTTOM_RIGHT);
        hbsendBut.setPadding(new Insets(0,10,10,0));
        hbsendBut.getChildren().addAll(sendPicBut,sendRecordBut,sendfileBut,sendBut);
        vbRight.getChildren().add(hbsendBut);
        grid.add(vbleft,0,0);
        grid.add(vbRight,1,0);
        addNewContacterBut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new AddNewFriendGUI(new Stage());
            }
        });
        showContactListBut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADFRIENDLIST,new LoadfriendlistInfo(id)));
            }
        });
        showGroupListBut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADGROUPLIST,new LoadgrouplistInfo(id)));
            }
        });
        ResponseHandler.getResHdler().setLoginListener(new ResponseHandler.LoginListener() {
            @Override
            public void doevent(int status) {
                ResponseObject resObj = ResponseHandler.getResHdler().getResObject();
                System.out.println(resObj);
                new Thread(new Task<Void>() {
                    @Override
                    protected void succeeded() {
                        switch (resObj.getResType()) {
                            case ResponseObject.RES_LOADFRIENDLIST:
                                LoadfriendlistInfoRes loadfriendlistInfoRes = (LoadfriendlistInfoRes) resObj.getResBody();
                                String[] num = new String[loadfriendlistInfoRes.getUsername().length];
                                for (int j = 0; j < num.length; j++)
                                    num[j] = Integer.toString(j);
                                ObservableList<String> strList = FXCollections.observableArrayList(num);
                                ListView<String> listView = new ListView<>(strList);
                                listView.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
                                    @Override
                                    public ListCell<String> call(ListView<String> param) {
                                        ContactCell contactCell = new ContactCell();
                                        contactCell.setUsername(loadfriendlistInfoRes.getUsername());
                                        contactCell.setUseronline(loadfriendlistInfoRes.getUseronline());
                                        return contactCell;
                                    }
                                });
                                listView.setPrefSize(2 * primaryStage.getWidth() / 7, primaryStage.getHeight());
                                if(vbleft.getChildren().size()==2){
                                    vbleft.getChildren().remove(1);
                                    vbleft.getChildren().add(listView);
                                }else{
                                    vbleft.getChildren().add(listView);
                                }
                                listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                                    @Override
                                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                        currChatuid = loadfriendlistInfoRes.getUserlist()[Integer.parseInt(newValue)];
                                        currChatGroupid = -1;
                                        chatContent.getChildren().clear();
                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADHISTORYCHAT,new LoadhistorychatInfo(currChatuid)));
                                        sendBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                if(!chatMessage.getText().isEmpty()) {
                                                    RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, new MessageInfo(currChatuid, chatMessage.getText(), MessageInfo.MESSAGETYPE_TEXTSENDFROMME)));
                                                    chatMessage.clear();
                                                }
                                            }
                                        });
                                        sendfileBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                File file = fileChooser.showOpenDialog(primaryStage);
                                                if(file == null){
                                                    return;
                                                }
                                                long maxfile = 16777215; //maxfile is 16MB
                                                String errmsg = null;
                                                if(file.length() >= maxfile){
                                                    errmsg = "The maxium of the file should be less than 16MB!";
                                                }else if(file.getName().length() > 30){
                                                    errmsg = "The max length of the filename \nshould be no more than 30!";
                                                }
                                                if(errmsg != null){
                                                    errDialog d = new errDialog("Send File Error!","Sorry!",errmsg,new Stage());
                                                    d.setOnClickListener(new errDialog.OnClickListener() {
                                                        @Override
                                                        public void OnClick() {
                                                            return;
                                                        }
                                                    });
                                                    return;
                                                }
                                                MessageInfo fileMessageInfo = new MessageInfo(currChatuid,null, MessageInfo.MESSAGETYPE_FILESENDFROMME);
                                                fileMessageInfo.setChatfile(file);
                                                fileMessageInfo.setChatfilelength(file.length());
                                                FileTransfer fileTransfer = new FileTransfer(new Stage(),FileTransfer.SEND);
                                                fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                                                    @Override
                                                    public void filetansevent() {
                                                        return;
                                                    }
                                                });
                                                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, fileMessageInfo));
                                                RequestHandler.getReqHdler().sendFile(file);
                                            }
                                        });
                                        sendRecordBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                new RecordGUI(new Stage(),currChatuid,false);
                                            }
                                        });
                                        sendPicBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                File picFile = fileChooser.showOpenDialog(primaryStage);
                                                if(picFile == null){
                                                    return;
                                                }
                                                long maxfile = 1048576; //maxfile is 10MB
                                                String errmsg = null;
                                                if(picFile.length() >= maxfile){
                                                    errmsg = "The maxium of the picture file should be less than 1MB!";
                                                }else if(picFile.getName().length() > 30){
                                                    errmsg = "The max length of the picture name \nshould be no more than 30!";
                                                }
                                                if(errmsg != null){
                                                    errDialog d = new errDialog("Send File Error!","Sorry!",errmsg,new Stage());
                                                    d.setOnClickListener(new errDialog.OnClickListener() {
                                                        @Override
                                                        public void OnClick() {
                                                            return;
                                                        }
                                                    });
                                                    return;
                                                }
                                                MessageInfo picFileMessageInfo = new MessageInfo(currChatuid,null, MessageInfo.MESSAGETYPE_PICSENDFROMME);
                                                picFileMessageInfo.setChatfile(picFile);
                                                picFileMessageInfo.setChatfilelength(picFile.length());
                                                FileTransfer fileTransfer = new FileTransfer(new Stage(),FileTransfer.SEND);
                                                fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                                                    @Override
                                                    public void filetansevent() {
                                                        return;
                                                    }
                                                });
                                                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, picFileMessageInfo));
                                                RequestHandler.getReqHdler().sendFile(picFile);
                                            }
                                        });
                                    }
                                });
                                createNewGroupBut.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        new CreateChatgroupGUI(new Stage(),loadfriendlistInfoRes.getUserlist(),loadfriendlistInfoRes.getUsername());
                                    }
                                });
                                break;
                            case ResponseObject.RES_ADDNEWFRIEND:
                                NewFriendInfoRes newFriendInfoRes = (NewFriendInfoRes) resObj.getResBody();
                                new AddNewFriendResGUI(new Stage(), newFriendInfoRes.getMessage());
                                break;
                            case ResponseObject.RES_SENDMESSAGE:
                                MessageInfo messageInfo = (MessageInfo)resObj.getResBody();
                                if(messageInfo.getReceiverid() == currChatuid) {
                                    new MessageGUIHandler(chatContent,messageInfo,primaryStage,false);
                                    chatContentPane.setVvalue(1);
                                }
                                break;
                            case ResponseObject.RES_LOADHISTORYCHAT:
                                LoadhistorychatInfoRes loadhistorychatInfoRes = (LoadhistorychatInfoRes)resObj.getResBody();
                                if(loadhistorychatInfoRes.getChatid() == currChatuid) {
                                    for (int i = 0; i < loadhistorychatInfoRes.getChatmessage().length;i++){
                                        messageInfo = loadhistorychatInfoRes.getChatmessage()[i];
                                        if(messageInfo.getReceiverid() == currChatuid) {
                                            new MessageGUIHandler(chatContent, messageInfo, primaryStage,false);
                                        }
                                    }
                                    chatContentPane.setVvalue(1);
                                }
                                break;
                            case ResponseObject.RES_LOADGROUPLIST:
                                LoadgrouplistInfoRes loadgrouplistInfoRes = (LoadgrouplistInfoRes) resObj.getResBody();
                                num = new String[loadgrouplistInfoRes.getGroupname().length];
                                for (int j = 0; j < num.length; j++)
                                    num[j] = Integer.toString(j);
                                strList = FXCollections.observableArrayList(num);
                                listView = new ListView<>(strList);
                                listView.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
                                    @Override
                                    public ListCell<String> call(ListView<String> param) {
                                        JoinedGroupCell joinedGroupCell = new JoinedGroupCell();
                                        joinedGroupCell.setGroupname(loadgrouplistInfoRes.getGroupname());
                                        joinedGroupCell.setGroupmembers(loadgrouplistInfoRes.getGroupmembers());
                                        return joinedGroupCell;
                                    }
                                });
                                listView.setPrefSize(2 * primaryStage.getWidth() / 7, primaryStage.getHeight());
                                if(vbleft.getChildren().size()==2){
                                    vbleft.getChildren().remove(1);
                                    vbleft.getChildren().add(listView);
                                }else{
                                    vbleft.getChildren().add(listView);
                                }
                                listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                                    @Override
                                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                        currChatGroupid = loadgrouplistInfoRes.getGrouplist()[Integer.parseInt(newValue)];
                                        currChatuid = -1;
                                        chatContent.getChildren().clear();
                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADGROUPHISTORYCHAT,new LoadhistorychatInfo(currChatGroupid)));
                                        sendBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                if(!chatMessage.getText().isEmpty()) {
                                                    RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, new MessageInfo(currChatGroupid, chatMessage.getText(), MessageInfo.MESSAGETYPE_TEXTSENDFROMME)));
                                                    chatMessage.clear();
                                                }
                                            }
                                        });
                                        sendfileBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                File file = fileChooser.showOpenDialog(primaryStage);
                                                if(file == null){
                                                    return;
                                                }
                                                long maxfile = 16777215; //maxfile is 16MB
                                                String errmsg = null;
                                                if(file.length() >= maxfile){
                                                    errmsg = "The maxium of the file should be less than 16MB!";
                                                }else if(file.getName().length() > 20){
                                                    errmsg = "The max length of the filename \nshould be no more than 20!";
                                                }
                                                if(errmsg != null){
                                                    errDialog d = new errDialog("Send File Error!","Sorry!",errmsg,new Stage());
                                                    d.setOnClickListener(new errDialog.OnClickListener() {
                                                        @Override
                                                        public void OnClick() {
                                                            return;
                                                        }
                                                    });
                                                    return;
                                                }
                                                MessageInfo fileMessageInfo = new MessageInfo(currChatGroupid,null,MessageInfo.MESSAGETYPE_FILESENDFROMME);
                                                fileMessageInfo.setChatfile(file);
                                                fileMessageInfo.setChatfilelength(file.length());
                                                FileTransfer fileTransfer = new FileTransfer(new Stage(),FileTransfer.SEND);
                                                fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                                                    @Override
                                                    public void filetansevent() {
                                                        return;
                                                    }
                                                });
                                                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, fileMessageInfo));
                                                RequestHandler.getReqHdler().sendFile(file);
                                            }
                                        });
                                        sendRecordBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                new RecordGUI(new Stage(),currChatGroupid,true);
                                            }
                                        });
                                        sendPicBut.setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                File picFile = fileChooser.showOpenDialog(primaryStage);
                                                if(picFile == null){
                                                    return;
                                                }
                                                long maxfile = 10485760; //maxfile is 10MB
                                                String errmsg = null;
                                                if(picFile.length() >= maxfile){
                                                    errmsg = "The maxium of the picture file should be less than 10MB!";
                                                }else if(picFile.getName().length() > 30){
                                                    errmsg = "The max length of the picture name \nshould be no more than 30!";
                                                }
                                                if(errmsg != null){
                                                    errDialog d = new errDialog("Send File Error!","Sorry!",errmsg,new Stage());
                                                    d.setOnClickListener(new errDialog.OnClickListener() {
                                                        @Override
                                                        public void OnClick() {
                                                            return;
                                                        }
                                                    });
                                                    return;
                                                }
                                                MessageInfo picFileMessageInfo = new MessageInfo(currChatGroupid,null,MessageInfo.MESSAGETYPE_PICSENDFROMME);
                                                picFileMessageInfo.setChatfile(picFile);
                                                picFileMessageInfo.setChatfilelength(picFile.length());
                                                FileTransfer fileTransfer = new FileTransfer(new Stage(),FileTransfer.SEND);
                                                fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                                                    @Override
                                                    public void filetansevent() {
                                                        return;
                                                    }
                                                });
                                                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, picFileMessageInfo));
                                                RequestHandler.getReqHdler().sendFile(picFile);
                                            }
                                        });
                                    }
                                });
                                break;
                            case ResponseObject.RES_SENDGROUPMESSAGE:
                                messageInfo = (MessageInfo)resObj.getResBody();
                                if(messageInfo.getReceiverid() == currChatGroupid) {
                                    new MessageGUIHandler(chatContent,messageInfo,primaryStage,true);
                                    chatContentPane.setVvalue(1);
                                }
                                break;
                            case ResponseObject.RES_LOADGROUPHISTORYCHAT:
                                loadhistorychatInfoRes = (LoadhistorychatInfoRes)resObj.getResBody();
                                if(loadhistorychatInfoRes.getChatid() == currChatGroupid) {
                                    for (int i = 0; i < loadhistorychatInfoRes.getChatmessage().length;i++){
                                        messageInfo = loadhistorychatInfoRes.getChatmessage()[i];
                                        if(messageInfo.getReceiverid() == currChatGroupid) {
                                            new MessageGUIHandler(chatContent, messageInfo, primaryStage,true);
                                        }
                                    }
                                    chatContentPane.setVvalue(1);
                                }
                                break;
                        }
                    }
                    @Override
                    protected Void call() throws Exception{
                        return null;
                    }
                }).start();
            }
        });
        RequestHandler.getReqHdler().setErrorListener(new RequestHandler.ErrorListener() {
            @Override
            public void errorevent() {
                errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
                d.setOnClickListener(new errDialog.OnClickListener() {
                    @Override
                    public void OnClick() {
                        System.exit(0);
                    }
                });
            }
        });
        ResponseHandler.getResHdler().setErrorListener(new ResponseHandler.ErrorListener() {
            @Override
            public void errorevent() {
                errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
                d.setOnClickListener(new errDialog.OnClickListener() {
                    @Override
                    public void OnClick() {
                        System.exit(0);
                    }
                });
            }
        });
        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADFRIENDLIST,new LoadfriendlistInfo(id)));
        ResponseHandler.getResHdler().readMessage();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true)
                    ResponseHandler.getResHdler().readMessage();
            }
        }).start();
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });
    }
}
