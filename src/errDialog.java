import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;


/**
 * Created by Administrator1 on 2016/11/9.
 */
public class errDialog {
    public errDialog(String title,String argsTitle,String args,Stage primaryStage){
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle(title);
        primaryStage.setResizable(false);
        int disWidth = 340;
        int disHeight = 270;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(20);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Button okBut = new Button("OK");
        okBut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.hide();
                _listener.OnClick();
            }
        });
        HBox hbokBut = new HBox();
        hbokBut.setAlignment(Pos.BOTTOM_RIGHT);
        hbokBut.getChildren().add(okBut);
        if(argsTitle != null){
            Text errMsgTitle = new Text(argsTitle);
            errMsgTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,30));
            grid.add(errMsgTitle,0,0,2,1);
            Text errMsg = new Text(args);
            grid.add(errMsg,0,1,3,1);
            grid.add(hbokBut,2,2);
        }else{
            Text errMsg = new Text(args);
            grid.add(errMsg,0,0,3,1);
            grid.add(hbokBut,2,1);
        }
    }
    private errDialog.OnClickListener _listener;
    public errDialog setOnClickListener(errDialog.OnClickListener listener){
        _listener = listener;
        return  errDialog.this;
    }

    public interface OnClickListener{
        void OnClick();
    }
}
