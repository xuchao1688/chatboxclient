import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Created by Administrator1 on 2016/12/24.
 */

public class CreateChatgroupGUI{
    private boolean[] selectedUser;
    public CreateChatgroupGUI(Stage primaryStage,int[] userList, String[] nameList){
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("Create new group!");
        primaryStage.setResizable(false);
        int disWidth = 400;
        int disHeight = 350;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(15);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Label scenetitle = new Label("New group name: ");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,16));
        grid.add(scenetitle,0,0,1,1);
        TextField tf = new TextField();
        grid.add(tf,2,0,3,1);
        Label listTitle = new Label("Choose group member:");
        grid.add(listTitle,0,1,4,1);
        ScrollPane listScrollPane = new ScrollPane();
        VBox listContent = new VBox();
        listScrollPane.setContent(listContent);
        listScrollPane.setPrefSize(370,200);
        grid.add(listScrollPane,0,2,5,1);
        selectedUser = new boolean[userList.length];
        CheckBox cbUser = null;
        for(int i  = 0; i < userList.length; i++){
            cbUser = new CheckBox(nameList[i]);
            listContent.getChildren().add(cbUser);
            final int index  = i;
            cbUser.selectedProperty().addListener((observable, oldValue, newValue) -> {
                selectedUser[index] = newValue;
            });
        }
        Button but1 = new Button("Cancel");
        grid.add(but1,0,3);
        Button but2 = new Button("OK");
        HBox hbbut2 = new HBox();
        hbbut2.setAlignment(Pos.BOTTOM_RIGHT);
        hbbut2.getChildren().add(but2);
        grid.add(hbbut2,4,3);
        Text errText = new Text();
        errText.setFill(Color.FIREBRICK);
        grid.add(errText,0,4,4,1);
        but1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.hide();
            }
        });
        but2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tf.getText().isEmpty()){
                    errText.setText("You must input Group Name!");
                    return;
                }
                int count = 0;
                for(int i = 0; i < selectedUser.length;i++){
                    if(selectedUser[i])
                        count++;
                }
                int[] selectedUserlist = new int[count];
                count = 0;
                for(int i = 0; i < selectedUser.length;i++){
                    if(selectedUser[i])
                        selectedUserlist[count++] = userList[i];
                }
                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_CREATENEWGROUP,new NewGroupInfo(tf.getText(),selectedUserlist)));
                primaryStage.hide();
            }
        });
    }
}
