import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/11/2.
 */
public class ResponseObject implements Serializable {
    public static final int RES_REG = 1;
    public static final int RES_LOG = 2;
    public static final int RES_LOADFRIENDLIST = 3;
    public static final int RES_ADDNEWFRIEND = 4;
    public static final int RES_SENDMESSAGE = 5;
    public static final int RES_LOADHISTORYCHAT = 6;
    public static final int RES_FILERECEIVE = 7;
    public static final int RES_LOADGROUPLIST = 8;
    public static final int RES_SENDGROUPMESSAGE = 9;
    public static final int RES_LOADGROUPHISTORYCHAT = 10;
    private int resType;
    private Object resBody;
    public ResponseObject(int resType,Object resBody){
        super();
        this.resType = resType;
        this.resBody = resBody;
    }
    public int getResType(){
        return resType;
    }
    public Object getResBody(){
        return resBody;
    }
    @Override
    public String toString(){
        return "Response Type: " + resType + " .Response Content: " + resBody;
    }
}
