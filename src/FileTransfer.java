import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.sql.ResultSet;

/**
 * Created by Administrator1 on 2016/12/21.
 */

public class FileTransfer {
    public static final int SEND = 1;
    public static final int RECEIVE = 2;
    private Text progressMsgText;
    private HBox hbokBut;
    private GridPane grid;
    public FileTransfer(Stage primaryStage,int trans){
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("File Transfer!");
        primaryStage.setResizable(false);
        int disWidth = 340;
        int disHeight = 270;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(20);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Button okBut = new Button("OK");
        okBut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.hide();
                _FileTransListener.filetansevent();
            }
        });
        hbokBut = new HBox();
        hbokBut.setAlignment(Pos.CENTER);
        hbokBut.getChildren().add(okBut);
        progressMsgText = new Text();
        grid.add(progressMsgText,0,0);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                return;
            }
        });
        RequestHandler.getReqHdler().setErrorListener(new RequestHandler.ErrorListener() {
            @Override
            public void errorevent() {
                errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
                d.setOnClickListener(new errDialog.OnClickListener() {
                    @Override
                    public void OnClick() {
                        System.exit(0);
                    }
                });
            }
        });
        switch (trans){
            case SEND:
                sendFile(primaryStage);
                break;
            case RECEIVE:
                receiveFile(primaryStage);
                break;
        }
    }
    private void sendFile(Stage primaryStage){
        RequestHandler.getReqHdler().setSendFileListener(new RequestHandler.SendFileListener() {
            @Override
            public void transFile(String progressMsg,boolean state) {
                new Thread(new Task<Void>() {
                    @Override
                    protected void succeeded(){
                        progressMsgText.setText("File Sending Progress:  \n"+progressMsg);
                        if(state){
                            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                                @Override
                                public void handle(WindowEvent event) {
                                    primaryStage.hide();
                                }
                            });
                            grid.add(hbokBut,0,1);
                        }
                    }
                    @Override
                    protected Void call() throws Exception{
                        return null;
                    }
                }).start();
            }
        });
    }

    private void receiveFile(Stage primaryStage){
        ResponseHandler.getResHdler().setReceiveFileListener(new ResponseHandler.ReceiveFileListener(){
            @Override
            public void transFile(String progressMsg,boolean state) {
                new Thread(new Task<Void>() {
                    @Override
                    protected void succeeded(){
                        progressMsgText.setText("File Receiving Progress:  \n"+progressMsg);
                        if(state){
                            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                                @Override
                                public void handle(WindowEvent event) {
                                    primaryStage.hide();
                                }
                            });
                            grid.add(hbokBut,0,1);
                        }
                    }
                    @Override
                    protected Void call() throws Exception{
                        return null;
                    }
                }).start();
            }
        });
    }
    private FileTransListener _FileTransListener;

    public FileTransfer setFileTransListener(FileTransListener fileTransListener){
        _FileTransListener = fileTransListener;
        return FileTransfer.this;
    }
    public interface FileTransListener{
        public void filetansevent();
    }
}
