import java.io.Serializable;
/**
 * Created by Administrator1 on 2016/12/25.
 */

public class LoadgrouplistInfoRes implements Serializable{
    private int[] grouplist;
    private String[] groupname;
    private int[] groupmembers;
    public LoadgrouplistInfoRes(int[] list,String[] name,int[] members){
        grouplist = list;
        groupname = name;
        groupmembers = members;
    }
    @Override
    public String toString(){
        return "group number is " + grouplist.length;
    }
    public int[] getGrouplist(){
        return grouplist;
    }
    public String[] getGroupname(){
        return groupname;
    }
    public int[] getGroupmembers(){
        return groupmembers;
    }
}
