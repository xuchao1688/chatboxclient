/**
 * Created by Administrator1 on 2016/11/8.
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.stage.*;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RegisterGUI {

    public RegisterGUI(Stage primaryStage) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("ChatBox Register!");
        primaryStage.setResizable(false);
        int disWidth = 440;
        int disHeight = 375;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Text scenetitle = new Text("Sign up");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,30));
        grid.add(scenetitle,0,0,2,1);
        Label lb1 = new Label("User Name:");
        grid.add(lb1,0,1);
        TextField tf1 = new TextField();
        grid.add(tf1,1,1);
        Label lb2 = new Label("Password:");
        grid.add(lb2,0,2);
        PasswordField pf1 = new PasswordField();
        grid.add(pf1,1,2);
        Label lb3 = new Label("Retype password:");
        grid.add(lb3,0,3);
        PasswordField pf2 = new PasswordField();
        grid.add(pf2,1,3);
        Button but1 = new Button("Back");
        grid.add(but1,0,5);
        but1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new LoginGUI(primaryStage);
            }
        });
        Button but2 = new Button("OK");
        HBox hbbut2 = new HBox();
        hbbut2.setAlignment(Pos.BOTTOM_RIGHT);
        hbbut2.getChildren().add(but2);
        grid.add(hbbut2,1,5);
        final Text errText  = new Text();
        errText.setFill(Color.FIREBRICK);
        grid.add(errText,1,7);
        ResponseHandler.getResHdler().setLoginListener(new ResponseHandler.LoginListener() {
            @Override
            public void doevent(int status) {
                ResponseObject resObj = ResponseHandler.getResHdler().getResObject();
                System.out.println(resObj);
                if(((RegInfoRes)resObj.getResBody()).getMessage().equals("successfully register")){
                    errDialog dd = new errDialog("ChatBox","Congratulations!","Succeed in registering!",new Stage());
                    dd.setOnClickListener(new errDialog.OnClickListener() {
                        @Override
                        public void OnClick() {
                            new LoginGUI(primaryStage);
                        }
                    });
                }else if(((RegInfoRes)resObj.getResBody()).getMessage().equals("fail to register")){
                    errText.setText("Sorry!This username has been used!");
                }
                else{
                    errText.setText("Sorry! Cannot register.\nPlease try again");
                }
            }
        });
        RequestHandler.getReqHdler().setErrorListener(new RequestHandler.ErrorListener() {
            @Override
            public void errorevent() {
                errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
                d.setOnClickListener(new errDialog.OnClickListener() {
                    @Override
                    public void OnClick() {
                        System.exit(0);
                    }
                });
            }
        });
        ResponseHandler.getResHdler().setErrorListener(new ResponseHandler.ErrorListener() {
            @Override
            public void errorevent() {
                errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
                d.setOnClickListener(new errDialog.OnClickListener() {
                    @Override
                    public void OnClick() {
                        System.exit(0);
                    }
                });
            }
        });
        but2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String args = null;
                if(!pf1.getText().equals(pf2.getText())){
                    args = "The confirm password is \nincorrect!";
                }
                if(pf1.getText().equals("")){
                    args = "You must input password!";
                }
                if(tf1.getText().equals("")){
                    args = "You must input username!";
                }
                errText.setText(args);
                if(args != null)
                    return;
                RequestObject reqObj = new RequestObject(RequestObject.REQ_REG,new RegInfo(tf1.getText(),pf1.getText()));
                RequestHandler.getReqHdler().sendMessage(reqObj);
                ResponseHandler.getResHdler().readMessage();
            }
        });
    }
}

