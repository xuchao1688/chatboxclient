import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 * Created by Administrator1 on 2016/12/17.
 */
public class MessageGUIHandler {
    public MessageGUIHandler(GridPane chatContent, MessageInfo messageInfo, Stage primaryStage, boolean state) {
        HBox hbChat = new HBox();
        Text chatmess = null;
        if(state){
            if(messageInfo.getMessagetype() == MessageInfo.MESSAGETYPE_TIMEINFO){
                chatmess = new Text(messageInfo.getChatmessage());
            }else {
                chatmess = new Text(messageInfo.getSendername() + ": " + messageInfo.getChatmessage());
            }
        }else {
            chatmess = new Text(messageInfo.getChatmessage());
        }
        chatmess.setWrappingWidth(4.6 * primaryStage.getWidth() /7);
        chatmess.maxWidth(4.6 * primaryStage.getWidth() /7);
        hbChat.setMaxWidth(4.6 * primaryStage.getWidth() / 7);
        hbChat.getChildren().add(chatmess);
        DirectoryChooser directoryChooser = new DirectoryChooser();
        int size = chatContent.getChildren().size();
        if (size == 0) {
            HBox hb1 = new HBox();
            hb1.setPrefSize(4.7 * primaryStage.getWidth() / 14, 1);
            HBox hb2 = new HBox();
            hb2.setPrefSize(4.7 * primaryStage.getWidth() / 14, 1);
            chatContent.add(hb1, 0, 0,2,1);
            chatContent.add(hb2, 1, 0,2,1);
        }
        switch (messageInfo.getMessagetype()) {
            case MessageInfo.MESSAGETYPE_TEXTSENDFROMOTHERS:
                hbChat.setAlignment(Pos.CENTER_LEFT);
                chatmess.setTextAlignment(TextAlignment.LEFT);
                chatContent.add(hbChat, 0, size,2,1);
                break;
            case MessageInfo.MESSAGETYPE_TEXTSENDFROMME:
                chatmess.setFill(Color.GREEN);
                chatmess.setTextAlignment(TextAlignment.RIGHT);
                hbChat.setAlignment(Pos.CENTER_RIGHT);
                chatContent.add(hbChat, 1, size,2,1);
                break;
            case MessageInfo.MESSAGETYPE_TIMEINFO:
                hbChat.setAlignment(Pos.CENTER);
                chatmess.setTextAlignment(TextAlignment.CENTER);
                chatContent.add(hbChat, 0, size,2,1);
                break;
            case MessageInfo.MESSAGETYPE_FILESENDFROMOTHERS:
                hbChat.setAlignment(Pos.CENTER_LEFT);
                chatmess.setTextAlignment(TextAlignment.LEFT);
                chatContent.add(hbChat, 0, size,2,1);
                Button fileReceiveBut = new Button("Receive File");
                fileReceiveBut.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        File fileDirectory = directoryChooser.showDialog(primaryStage);
                        if(fileDirectory == null){
                            return;
                        }
                        File file = new File(fileDirectory.getPath() + messageInfo.getChatfile().getName());
                        ResponseHandler.getResHdler().setReceiveFile(file);
                        ResponseHandler.getResHdler().setFileLength(messageInfo.getChatfilelength());
                        MessageInfo fileReceiveInfo = new MessageInfo(0,"",MessageInfo.MESSAGETYPE_FILERECEIVE);
                        fileReceiveInfo.setChatfileid(messageInfo.getChatfileid());
                        fileReceiveInfo.setChatfile(messageInfo.getChatfile());
                        FileTransfer fileTransfer= new FileTransfer(new Stage(),FileTransfer.RECEIVE);
                        fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                            @Override
                            public void filetansevent() {
                                return;
                            }
                        });
                        if(state){
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, fileReceiveInfo));
                        }else {
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, fileReceiveInfo));
                        }
                    }
                });
                HBox hbfileReceiveBut = new HBox();
                hbfileReceiveBut.setAlignment(Pos.CENTER_LEFT);
                hbfileReceiveBut.getChildren().add(fileReceiveBut);
                chatContent.add(hbfileReceiveBut,0,size+1,2,1);
                break;
            case MessageInfo.MESSAGETYPE_FILESENDFROMME:
                chatmess.setFill(Color.GREEN);
                chatmess.setTextAlignment(TextAlignment.RIGHT);
                hbChat.setAlignment(Pos.CENTER_RIGHT);
                chatContent.add(hbChat, 1, size,2,1);
                fileReceiveBut = new Button("Receive File");
                fileReceiveBut.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        File fileDirectory = directoryChooser.showDialog(primaryStage);
                        if(fileDirectory == null){
                            return;
                        }
                        File file = new File(fileDirectory.getPath() + messageInfo.getChatfile().getName());
                        ResponseHandler.getResHdler().setReceiveFile(file);
                        ResponseHandler.getResHdler().setFileLength(messageInfo.getChatfilelength());
                        MessageInfo fileReceiveInfo = new MessageInfo(0,"",MessageInfo.MESSAGETYPE_FILERECEIVE);
                        fileReceiveInfo.setChatfileid(messageInfo.getChatfileid());
                        fileReceiveInfo.setChatfile(messageInfo.getChatfile());
                        FileTransfer fileTransfer = new FileTransfer(new Stage(),FileTransfer.RECEIVE);
                        fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                            @Override
                            public void filetansevent() {
                                return;
                            }
                        });
                        if(state){
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, fileReceiveInfo));
                        }else {
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, fileReceiveInfo));
                        }
                    }
                });
                hbfileReceiveBut = new HBox();
                hbfileReceiveBut.setAlignment(Pos.CENTER_RIGHT);
                hbfileReceiveBut.getChildren().add(fileReceiveBut);
                chatContent.add(hbfileReceiveBut,1,size+1,2,1);
                break;
            case MessageInfo.MESSAGETYPE_PICSENDFROMOTHERS:
                hbChat.setAlignment(Pos.CENTER_LEFT);
                chatmess.setTextAlignment(TextAlignment.LEFT);
                chatContent.add(hbChat, 0, size,2,1);
                Button picReceiveBut = new Button("Receive Pic");
                HBox hbpicReceiveBut = new HBox();
                hbpicReceiveBut.setAlignment(Pos.CENTER_LEFT);
                hbpicReceiveBut.getChildren().add(picReceiveBut);
                chatContent.add(hbpicReceiveBut,0,size+1,2,1);
                picReceiveBut.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        File dir = new File("./temppics");
                        if(!dir.exists()){
                            dir.mkdir();
                        }
                        File file = new File("./temppics/" + messageInfo.getChatfile().getName());
                        ResponseHandler.getResHdler().setReceiveFile(file);
                        ResponseHandler.getResHdler().setFileLength(messageInfo.getChatfilelength());
                        MessageInfo fileReceiveInfo = new MessageInfo(0,"",MessageInfo.MESSAGETYPE_FILERECEIVE);
                        fileReceiveInfo.setChatfileid(messageInfo.getChatfileid());
                        fileReceiveInfo.setChatfile(messageInfo.getChatfile());
                        FileTransfer fileTransfer = new FileTransfer(new Stage(),FileTransfer.RECEIVE);
                        fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                            @Override
                            public void filetansevent() {
                                Image pic = new Image("file:temppics/"+messageInfo.getChatfile().getName());
                                ImageView iv = new ImageView(pic);
                                iv.setPreserveRatio(true);
                                if(pic.getWidth() > 4 * primaryStage.getWidth() /7){
                                    iv.setFitWidth(4 * primaryStage.getWidth() /7);
                                }
                                hbpicReceiveBut.getChildren().clear();
                                hbpicReceiveBut.getChildren().add(iv);
                            }
                        });
                        if(state){
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, fileReceiveInfo));
                        }else {
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, fileReceiveInfo));
                        }
                    }
                });
                break;
            case MessageInfo.MESSAGETYPE_PICSENDFROMME:
                chatmess.setFill(Color.GREEN);
                chatmess.setTextAlignment(TextAlignment.RIGHT);
                hbChat.setAlignment(Pos.CENTER_RIGHT);
                chatContent.add(hbChat, 1, size,2,1);
                picReceiveBut = new Button("Receive Pic");
                hbpicReceiveBut = new HBox();
                hbpicReceiveBut.setAlignment(Pos.CENTER_RIGHT);
                hbpicReceiveBut.getChildren().add(picReceiveBut);
                chatContent.add(hbpicReceiveBut,1,size+1,2,1);
                picReceiveBut.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        File dir = new File("./temppics");
                        if(!dir.exists()){
                            dir.mkdir();
                        }
                        File file = new File("./temppics/" + messageInfo.getChatfile().getName());
                        ResponseHandler.getResHdler().setReceiveFile(file);
                        ResponseHandler.getResHdler().setFileLength(messageInfo.getChatfilelength());
                        MessageInfo fileReceiveInfo = new MessageInfo(0,"",MessageInfo.MESSAGETYPE_FILERECEIVE);
                        fileReceiveInfo.setChatfileid(messageInfo.getChatfileid());
                        fileReceiveInfo.setChatfile(messageInfo.getChatfile());
                        FileTransfer fileTransfer = new FileTransfer(new Stage(),FileTransfer.RECEIVE);
                        fileTransfer.setFileTransListener(new FileTransfer.FileTransListener() {
                            @Override
                            public void filetansevent() {
                                Image pic = new Image("file:temppics/"+messageInfo.getChatfile().getName());
                                ImageView iv = new ImageView(pic);
                                iv.setPreserveRatio(true);
                                if(pic.getWidth() > 4 * primaryStage.getWidth() /7){
                                    iv.setFitWidth(4 * primaryStage.getWidth() /7);
                                }
                                HBox hbiv = new HBox();
                                hbiv.setAlignment(Pos.CENTER_RIGHT);
                                hbiv.getChildren().add(iv);
                                hbpicReceiveBut.getChildren().clear();
                                hbpicReceiveBut.getChildren().add(iv);
                            }
                        });
                        if(state){
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, fileReceiveInfo));
                        }else {
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, fileReceiveInfo));
                        }
                    }
                });
                break;
        }
    }
}
