import java.io.Serializable;

public class LogInfoRes implements Serializable{
    private String message;
    private int mid;
    public LogInfoRes(String mess, int id){
        message = mess;
        mid = id;
    }
    @Override
    public String toString(){
        return "message is '" + message +"',"+ "id is '"+ mid +"'.";
    }
    public String getMessage(){
        return message;
    }
    public int getID(){
        return mid;
    }
}
