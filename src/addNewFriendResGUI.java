/**
 * Created by Administrator1 on 2016/11/27.
 */
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class AddNewFriendResGUI {
    public AddNewFriendResGUI(Stage primaryStage, String username){
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("New friend invite!");
        primaryStage.setResizable(false);
        int disWidth = 330;
        int disHeight = 240;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(25);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Label scenetitle = new Label(username + " wants to \nbecome your friend!");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,16));
        grid.add(scenetitle,0,0,3,1);
        Button but1 = new Button("Ignore");
        grid.add(but1,0,1);
        Button but2 = new Button("Add him/her");
        HBox hbbut2 = new HBox();
        hbbut2.setAlignment(Pos.BOTTOM_RIGHT);
        hbbut2.getChildren().add(but2);
        grid.add(hbbut2,2,1);
        but1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.hide();
            }
        });
        but2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                RequestObject reqObj = new RequestObject(RequestObject.REQ_CONFIRMADDNEWFRIEND, new NewFriendInfo(username));
                RequestHandler.getReqHdler().sendMessage(reqObj);
                primaryStage.hide();
            }
        });
    }
}
