/**
 * Created by Administrator1 on 2016/11/27.
 */
import java.io.Serializable;

public class NewFriendInfoRes implements Serializable{
    private String message;
    public NewFriendInfoRes(String mess){
        message = mess;
    }
    @Override
    public String toString(){
        return "message is '" + message +"'";
    }
    public String getMessage(){
        return message;
    }
}