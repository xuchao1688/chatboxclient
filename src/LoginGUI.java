/**
 * Created by Administrator1 on 2016/11/8.
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.stage.*;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class LoginGUI {

    public LoginGUI(Stage primaryStage) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("ChatBox Welcome!");
        primaryStage.setResizable(false);
        int disWidth = 440;
        int disHeight = 345;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(15);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,30));
        grid.add(scenetitle,0,0,2,1);
        Label lb1 = new Label("User Name:");
        grid.add(lb1,0,1);
        TextField tf1 = new TextField();
        grid.add(tf1,1,1);
        Label lb2 = new Label("Password:");
        grid.add(lb2,0,2);
        PasswordField pf = new PasswordField();
        grid.add(pf,1,2);
        Button but1 = new Button("Register");
        but1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.hide();
                new RegisterGUI(new Stage());
            }
        });
        grid.add(but1,0,4);
        Button but2 = new Button("Log in");
        HBox hbbut2 = new HBox();
        hbbut2.setAlignment(Pos.BOTTOM_RIGHT);
        hbbut2.getChildren().add(but2);
        grid.add(hbbut2,1,4);
        final Text errText  = new Text();
        errText.setFill(Color.FIREBRICK);
        grid.add(errText,1,6);
        ResponseHandler.getResHdler().setLoginListener(new ResponseHandler.LoginListener() {
            @Override
            public void doevent(int status) {
                if(status == ResponseObject.RES_LOG){
                    ResponseObject resObj = ResponseHandler.getResHdler().getResObject();
                    System.out.println(resObj);
                    if(((LogInfoRes)resObj.getResBody()).getMessage().equals("successfully login")){
                        new ChatGUI(primaryStage,((LogInfoRes)resObj.getResBody()).getID());
                    }else if(((LogInfoRes)resObj.getResBody()).getMessage().equals("fail to login")){
                        errText.setText("Please input right \nusername or password");
                    }else{
                        errText.setText("Sorry! Cannot login.\nPlease try again");
                    }
                }
            }
        });
        RequestHandler.getReqHdler().setErrorListener(new RequestHandler.ErrorListener() {
            @Override
            public void errorevent() {
                errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
                d.setOnClickListener(new errDialog.OnClickListener() {
                    @Override
                    public void OnClick() {
                        System.exit(0);
                    }
                });
            }
        });
        ResponseHandler.getResHdler().setErrorListener(new ResponseHandler.ErrorListener() {
            @Override
            public void errorevent() {
                errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
                d.setOnClickListener(new errDialog.OnClickListener() {
                    @Override
                    public void OnClick() {
                        System.exit(0);
                    }
                });
            }
        });
        but2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String args = null;
                if(pf.getText().equals("")){
                    args = "You must input Password!";
                }
                if(tf1.getText().equals("")){
                    args = "You must input User Name!";
                }
                errText.setText(args);
                if(args != null)
                    return;
                RequestObject reqObj = new RequestObject(RequestObject.REQ_LOG,new LogInfo(tf1.getText(),pf.getText()));
                RequestHandler.getReqHdler().sendMessage(reqObj);
                ResponseHandler.getResHdler().readMessage();
            }
        });
    }
}
