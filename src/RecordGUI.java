import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator1 on 2016/12/27.
 */

public class RecordGUI{
    private int click = 0;
    private Date recordTime;
    private SimpleDateFormat timeFormat;
    private TargetDataLine targetDataLine;
    public RecordGUI(Stage primaryStage, int toid, boolean isGroup){
        try {
            timeFormat = new SimpleDateFormat("HH:mm:ss");
            recordTime = timeFormat.parse("00:00:00");
        }catch (Exception e){
            e.printStackTrace();
        }
        File file = new File("./tempVoiceFile");
        if(!file.exists()){
            file.mkdir();
        }
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setTitle("Send Voice!");
        primaryStage.setResizable(false);
        int disWidth = 450;
        int disHeight = 240;
        primaryStage.setX(bounds.getWidth()/2-1.f*disWidth/2);
        primaryStage.setY(bounds.getHeight()/2-1.f*disHeight/2);
        primaryStage.setWidth(disWidth);
        primaryStage.setHeight(disHeight);
        primaryStage.show();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(30);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid,primaryStage.getWidth(),primaryStage.getHeight());
        primaryStage.setScene(scene);
        Label scenetitle = new Label("Record time: ");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,16));
        grid.add(scenetitle,0,0,1,1);
        Text timeText = new Text();
        grid.add(timeText,1,0,2,1);
        Button but1 = new Button("Back");
        grid.add(but1,0,1);
        Button but2 = new Button("Start Record");
        grid.add(but2,1,1);
        Button but3 = new Button("Send Voice");
        but3.setVisible(false);
        grid.add(but3,2,1);
        Text mesText = new Text();
        grid.add(mesText,0,2,3,1);
        TimerTask timerTask = new TimerTask(){
            @Override
            public void run() {
                recordTime.setTime(recordTime.getTime() + 1000);
                timeText.setText(timeFormat.format(recordTime));
            }
        };
        Timer timer = new Timer();
        Date sysDate = new Date();
        File voiceFile = new File("./tempVoiceFile/" + sysDate.getTime() + ".wav");
        AudioFormat audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100F, 8, 1, 1, 44100F, false);
        DataLine.Info info = new DataLine.Info(TargetDataLine.class,audioFormat);
        but1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(voiceFile.isFile() && voiceFile.exists()){
                    voiceFile.delete();
                }
                primaryStage.hide();
            }
        });
        but2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                switch (click){
                    case 0:
                        timer.schedule(timerTask,0,1000);
                        try{
                            targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
                            targetDataLine.open(audioFormat);
                            targetDataLine.start();
                            but1.setDisable(true);
                            but3.setDisable(true);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        AudioSystem.write(new AudioInputStream(targetDataLine), AudioFileFormat.Type.WAVE, voiceFile);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }catch (LineUnavailableException e){
                            e.printStackTrace();
                            mesText.setText("No device found to record voice!");
                            mesText.setFill(Color.RED);
                        }catch (Exception ee){
                            ee.printStackTrace();
                            mesText.setText("Error happened when recording voice!");
                            mesText.setFill(Color.RED);
                        }
                        break;
                    case 1:
                        timer.cancel();
                        try{
                            targetDataLine.stop();
                            targetDataLine.close();
                        }catch (Exception e){
                            e.printStackTrace();
                            mesText.setText("Error happened when recording voice!");
                            mesText.setFill(Color.RED);
                        }
                        but1.setDisable(false);
                        but2.setDisable(true);
                        but3.setDisable(false);
                        but3.setVisible(true);
                        break;
                }
                click = click + 1;
            }
        });
        but3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MessageInfo fileMessageInfo = new MessageInfo(toid,null,MessageInfo.MESSAGETYPE_FILESENDFROMME);
                fileMessageInfo.setChatfile(voiceFile);
                fileMessageInfo.setChatfilelength(voiceFile.length());
                new FileTransfer(new Stage(),FileTransfer.SEND);
                if(isGroup) {
                    RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDGROUPMESSAGE, fileMessageInfo));
                }else {
                    RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_SENDMESSAGE, fileMessageInfo));
                }
                RequestHandler.getReqHdler().sendFile(voiceFile);
            }
        });
    }
}
