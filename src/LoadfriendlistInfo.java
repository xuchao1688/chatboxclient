import java.io.Serializable;

public class LoadfriendlistInfo implements Serializable{
    private int mid;
    public LoadfriendlistInfo(int id ){
        mid = id;
    }
    @Override
    public String toString(){
        return "myid is " + mid;
    }
    public int getID(){
        return mid;
    }

}
