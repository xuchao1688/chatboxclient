import java.io.*;

/**
 * Created by Administrator1 on 2016/11/20.
 */
public class RequestHandler {
    private ObjectOutputStream oos;
    private DataOutputStream dos;
    private static RequestHandler reqHdler = null;
    public static RequestHandler getReqHdler(){
        if(reqHdler == null)
            reqHdler = new RequestHandler();
        return  reqHdler;
    }

    public void sendMessage(RequestObject reqObj){
        try{
            oos.writeObject(reqObj);
        }catch (Exception e){
            e.printStackTrace();
            _ErrorListener.errorevent();
        }
    }
    public void setObjectOutputStream(ObjectOutputStream oos){
        this.oos = oos;
    }

    public void setDataOutputStream(DataOutputStream dos){
        this.dos = dos;
    }

    private ErrorListener _ErrorListener = null;

    public RequestHandler setErrorListener(ErrorListener errorListener){
        _ErrorListener = errorListener;
        return RequestHandler.getReqHdler();
    }
    public interface ErrorListener{
        public void errorevent();
    }

    public void sendFile(File file){
        try{
            FileInputStream fis = new FileInputStream(file);
            byte[] sendBytes = new byte[1024];
            int l = 0;
            long sum = 0;
            while(fis.available() > 0){
                l = fis.read(sendBytes,0,sendBytes.length);
                sum += l;
                _SendFileListener.transFile(sum+"/"+file.length()+"   " + 100 * sum / file.length() +"%",false);
                dos.write(sendBytes,0,l);
                dos.flush();
            }
            _SendFileListener.transFile(sum+"/"+file.length()+"   " + 100 * sum / file.length() +"%",true);
            fis.close();
        }catch (IOException e){
            e.printStackTrace();
            _ErrorListener.errorevent();
        }
    }

    private SendFileListener _SendFileListener = null;

    public RequestHandler setSendFileListener(SendFileListener sendFileListener){
        _SendFileListener = sendFileListener;
        return RequestHandler.getReqHdler();
    }
    public interface SendFileListener{
        public void transFile(String progressMsg,boolean state);
    }
}
