import javafx.application.Application;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.ConnectException;
import java.net.Socket;

/**
 * Created by Administrator1 on 2016/10/30.
 */
public class Main extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage){
        try {
            Socket socket = new Socket("chat.isdas.cn", 2333);
            OutputStream os = socket.getOutputStream();
            InputStream is = socket.getInputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            ObjectInputStream ois = new ObjectInputStream(is);
            DataOutputStream dos = new DataOutputStream(os);
            DataInputStream dis = new DataInputStream(is);
            RequestHandler.getReqHdler().setObjectOutputStream(oos);
            RequestHandler.getReqHdler().setDataOutputStream(dos);
            ResponseHandler.getResHdler().setObjectInputStream(ois);
            ResponseHandler.getResHdler().setDataInputStream(dis);
            new LoginGUI(primaryStage);
        }catch (Exception e){
            e.printStackTrace();
            errDialog d = new errDialog("ChatBox Error!","Sorry!","Something error happened.\nPlease check your network.",new Stage());
            d.setOnClickListener(new errDialog.OnClickListener() {
                @Override
                public void OnClick() {
                    System.exit(0);
                }
            });
        }
    }

}
