import java.io.*;

/**
 * Created by Administrator1 on 2016/11/22.
 */
public class ResponseHandler {
    private ObjectInputStream ois;
    private DataInputStream dis;
    private ResponseObject resObject;
    private File receiveFile;
    private long fileLength;
    private static ResponseHandler resHdler = null;
    public static ResponseHandler getResHdler(){
        if(resHdler == null)
            resHdler = new ResponseHandler();
        return  resHdler;
    }

    public void readMessage(){
        try{
            resObject = (ResponseObject) ois.readObject();
            _LoginListener.doevent(resObject.getResType());
            if(resObject.getResType() == ResponseObject.RES_FILERECEIVE){
                readFile(receiveFile,fileLength);
            }
        }catch (Exception e){
            e.printStackTrace();
            _ErrorListener.errorevent();
        }
    }
    public void setObjectInputStream(ObjectInputStream ois){
        this.ois = ois;
    }

    public void setDataInputStream(DataInputStream dis){
        this.dis = dis;
    }

    public void setReceiveFile(File file){
        receiveFile = file;
    }
    public void setFileLength(long length){
        fileLength = length;
    }
    public ResponseObject getResObject(){
        return resObject;
    }

    private LoginListener _LoginListener = null;

    public ResponseHandler setLoginListener(LoginListener loginListener){
        _LoginListener = loginListener;
        return ResponseHandler.getResHdler();
    }

    public interface LoginListener{
        public void doevent(int status);
    }

    private ResponseHandler.ErrorListener _ErrorListener = null;

    public ResponseHandler setErrorListener(ErrorListener errorListener){
        _ErrorListener = errorListener;
        return ResponseHandler.getResHdler();
    }
    public interface ErrorListener{
        public void errorevent();
    }

    private void readFile(File file,long length){
        try{
            FileOutputStream fos = new FileOutputStream(file);
            byte[] receiveBytes = new byte[1024];
            int l = 0;
            long sum = 0;
            while((l = dis.read(receiveBytes,0,receiveBytes.length)) > 0){
                sum += l;
                _ReceiveFileListener.transFile(sum+"/"+length+"   " + 100 * sum / length +"%",false);
                fos.write(receiveBytes,0,l);
                fos.flush();
                if(sum >= length){
                    break;
                }
            }
            _ReceiveFileListener.transFile(sum+"/"+length+"   " + 100 * sum / length +"%",true);
            fos.close();
        }catch (IOException e){
            e.printStackTrace();
            _ErrorListener.errorevent();
        }
    }
    private ReceiveFileListener _ReceiveFileListener = null;

    public ResponseHandler setReceiveFileListener(ReceiveFileListener receiveFileListener){
        _ReceiveFileListener = receiveFileListener;
        return ResponseHandler.getResHdler();
    }
    public interface ReceiveFileListener{
        public void transFile(String progressMsg,boolean state);
    }
}